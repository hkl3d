\input texinfo   @c -*-texinfo-*-
@comment %**start of header
@setfilename hkl3d.info
@include version.texi
@settitle Hkl3d Anti-Collision Diffraction Library @value{VERSION}
@syncodeindex pg cp
@comment %**end of header
@copying
This manual is for hkl3d Library (version @value{VERSION}, @value{UPDATED}).

Copyright @copyright{} 2010 Synchrotron SOLEIL
                       L'Orme des Merisiers Saint-Aubin
                       BP 48 91192 GIF-sur-YVETTE CEDEX

@quotation
The hkl3d library is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The hkl3d library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the hkl library.  If not, see <http://www.gnu.org/licenses/>.
@end quotation
@end copying

@dircategory Software libraries
@direntry
* hkl3d: (hkl3d).           Library for anti-collision diffractometer computation.
@end direntry

@titlepage
@title hkl3d Library
@subtitle for version @value{VERSION}, @value{UPDATED}
@author F-E. Picca (@email{picca@@synchrotorn-soleil.fr})
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents

@ifnottex
@node Top, Introduction, (dir), (dir)
@top hkl Diffraction Library

This manual is for Hkl Diffraction Library (version @value{VERSION}, @value{UPDATED}).
@end ifnottex

@menu
* Introduction::                
* Developpement::               
@end menu

@node Introduction, Developpement, Top, Top
@chapter Introduction

The purpose of the library is to factories anti-collision for diffractometers
of different geometries. It is used at the SOLEIL.

@node Developpement,  , Introduction, Top
@chapter Developpement

@section Getting hkl

To get hkl, you can download the last stable version from sourceforge or if you
want the latest development version use @uref{http://git.or.cz/, git} or
@uref{http://code.google.com/p/msysgit/downloads/list, msysgit} on windows system and
do
@example
$ git clone git://repo.or.cz/hkl3d.git
@end example
or
@example
$ git clone http://repo.or.cz/r/hkl3d.git (slower)
@end example
then checkout the next branch like this.
@example
$ cd hkl
$ git checkout -b next origin/next
@end example

@section Building hkl

To build hkl you need @uref{http://www.python.org, Python 2.3+} and the
@uref{http://www.gnu.org/software/gsl/, GNU Scientific Library 1.12+}
@example
$ ./waf configure
$ ./waf
$ ./waf install (as root)
@end example

This command compile the library and the test suit if everythings goes fine you
must have a @file{libhkl3d.so.@value{VERSION}} or @file{libhkl3d.lib} depending on your
platform in the @file{build/default/src} directory. If your platform is not supported yet please
contact the @email{picca@@synchrotron-soleil.fr}.

@section Hacking hkl

you can send your patch to the @email{picca@@synchrotron-soleil.fr} using
git

The developpement process is like this. suppose you wan to add a new feature to
hkl create first a new branch from the next one
@example
$ git checkout -b my-next next
@end example
then work...
@example
$ git commit -a
@end example
more work...
@example
$ git commit -a
@end example
now that your great feature is ready for publication, you can send by mail your
patches process like this:
@example
$ git format-patch origin/next
@end example
and send files @file{0001_xxx}  and @file{0002_xxx} created to the author.

@bye
