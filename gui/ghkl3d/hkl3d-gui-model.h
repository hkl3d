/*
 * This file is part of the hkl3d library.
 * inspired from logo-model.c of the GtkGLExt logo models.
 * written by Naofumi Yasufuku  <naofumi@users.sourceforge.net>
 *
 * The hkl library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The hkl library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the hkl library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2010      Synchrotron SOLEIL
 *                         L'Orme des Merisiers Saint-Aubin
 *                         BP 48 91192 GIF-sur-YVETTE CEDEX
 *
 * Authors: Oussama Sboui <oussama.sboui@synchrotron-soleil.fr>
 *          Picca Frédéric-Emmanuel <picca@synchrotron-soleil.fr>
 */

#ifndef __HKL3D_GUI_MODEL_H__
#define __HKL3D_GUI_MODEL_H__

#include <gtkmm.h>
#include <gtkglmm.h>

#include "GlutDemoApplication.h"
#include "hkl3d.h"

///////////////////////////////////////////////////////////////////////////////
//
// Logo classes.
//
///////////////////////////////////////////////////////////////////////////////

namespace Logo
{
	//
	// LogoModel class.
	//
	class LogoModel: public GlutDemoApplication
	{
	public:
		LogoModel(Hkl3D & hkl3d);
		virtual ~LogoModel(void);
		void model_draw (void);
		void drawSphere(void);
		void initPhysics(void);
		virtual void clientMoveAndDisplay(void);
		virtual void displayCallback(void);
	private:
		Hkl3D & _hkl3d;
	};

	//
	// Model class.
	//
	class Model
	{
		friend class Scene;
		friend class LogoModel;

	public:
		enum DisplayList {
			MODEL = 1,
			
		};

		static const float MAT_SPECULAR[4];
		static const float MAT_SHININESS[1];
		static const float MAT_BLACK[4];
		static const float MAT_RED[4];
		static const float MAT_GREEN[4];
		static const float MAT_BLUE[4];

		static const unsigned int DEFAULT_ROT_COUNT;

	public:
   
		explicit Model(Hkl3D & hkl3d, unsigned int rot_count = DEFAULT_ROT_COUNT,
				bool enable_anim = true, bool enable_wireframe=false);
		virtual ~Model(void);

	private:
		void init_gl(LogoModel* LogoM);

	public:
		void draw(void);

		void enable_anim(void)
		{
			m_EnableAnim = true;
		}
    		void enable_wireframe(void)
		{
			m_EnableWireframe = true;
		}
		void disable_anim(void)
		{
			m_EnableAnim = false;
		}
		void disable_wireframe(void)
		{
			m_EnableWireframe = false;
		}
		bool anim_is_enabled(void) const
		{
			return m_EnableAnim;
		}
		bool wireframe_is_enabled(void) const
		{
			return m_EnableWireframe;
		}
		void reset_anim(void);

		void set_pos(float x, float y, float z)
		{
			m_Pos[0] = x;
			m_Pos[1] = y;
			m_Pos[2] = z;
		}

		void set_quat(float q0, float q1, float q2, float q3)
		{
			m_Quat[0] = q0;
			m_Quat[1] = q1;
			m_Quat[2] = q2;
			m_Quat[3] = q3;
		}

	private:
		Hkl3D & _hkl3d;

		LogoModel* logoM;
		// Rotation mode.
		struct RotMode
		{
			float *axis;
			float sign;
		};

		static const RotMode ROT_MODE[];

	private:
		unsigned int m_RotCount;
		
		bool m_EnableWireframe;
		bool m_EnableAnim;
		unsigned int m_Mode;
		unsigned int m_Counter;

		float m_Pos[3];
		float m_Quat[4];
	};

} // namespace Logo

#endif // __HKL3D_GUI_MODEL_H__
